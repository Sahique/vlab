CREATE DATABASE  IF NOT EXISTS `vlab` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `vlab`;
-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: vlab
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `container_list`
--

DROP TABLE IF EXISTS `container_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `container_list` (
  `container_id` int NOT NULL,
  `container_type` varchar(45) DEFAULT NULL,
  `container_name` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`container_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `container_list`
--

LOCK TABLES `container_list` WRITE;
/*!40000 ALTER TABLE `container_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `container_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `container_schedular`
--

DROP TABLE IF EXISTS `container_schedular`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `container_schedular` (
  `id` int NOT NULL AUTO_INCREMENT,
  `creator_name` varchar(45) DEFAULT NULL,
  `course_name` varchar(45) DEFAULT NULL,
  `lab_type` varchar(45) DEFAULT NULL,
  `start_date` varchar(45) DEFAULT NULL,
  `stop_date` varchar(45) DEFAULT NULL,
  `no_of_workbench` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `buffer_start_datime` varchar(45) DEFAULT NULL,
  `buffer_stop_datime` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `container_schedular`
--

LOCK TABLES `container_schedular` WRITE;
/*!40000 ALTER TABLE `container_schedular` DISABLE KEYS */;
INSERT INTO `container_schedular` VALUES (1,'John mathew','Embedded Systems','Micro Controller lab','2023-09-28T09:00:32.955Z','2023-10-28T21:00:32.955Z','15','inactive',NULL,NULL),(2,'Ana mendis','Embedded Systems','HDL lab','2023-09-28T10:00:32.955Z','2023-10-28T11:00:32.955Z','4','inactive',NULL,NULL),(6,'Mohammed Sahique','AIM','cloud computing','2023-10-04T15:00:00.000Z','2023-10-04T16:00:00.000Z','10','inactive',NULL,NULL),(12,'John mathew','PCMB','phtsics lab','2023-10-28T10:00:00.000Z','2023-10-28T11:00:00.000Z','5','inactive',NULL,NULL),(13,'John mathew','Embedded Systems','Micro Controller lab','2023-09-28T17:49:32.955Z','2023-10-28T17:49:32.955Z','15','inactive',NULL,NULL),(14,'John mathew','Embedded Systems','Micro Controller lab','2023-10-22T17:00:00.955Z','2023-10-28T17:00:00.955Z','15','inactive',NULL,NULL),(15,'sahiq','Embedded Systems','DSP','2023-10-20T14:00:00.955Z','2023-10-25T15:00:00.955Z','10','inactive','2023-10-20 14:15:00.955000','2023-10-25 14:45:00.955000');
/*!40000 ALTER TABLE `container_schedular` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coresandstorage`
--

DROP TABLE IF EXISTS `coresandstorage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `coresandstorage` (
  `Sno` int NOT NULL,
  `coreallocated` int DEFAULT NULL,
  `corecredit` int DEFAULT NULL,
  `corebalance` int DEFAULT NULL,
  `storageallocated` int DEFAULT NULL,
  `storagecredit` int DEFAULT NULL,
  `stroragebalance` int DEFAULT NULL,
  `cpu` int DEFAULT NULL,
  `ram` int DEFAULT NULL,
  `totalcores` int DEFAULT NULL,
  `totalstroage` int DEFAULT NULL,
  PRIMARY KEY (`Sno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='								';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coresandstorage`
--

LOCK TABLES `coresandstorage` WRITE;
/*!40000 ALTER TABLE `coresandstorage` DISABLE KEYS */;
INSERT INTO `coresandstorage` VALUES (1,0,200,0,0,0,100000,0,NULL,200,100000);
/*!40000 ALTER TABLE `coresandstorage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `labtype`
--

DROP TABLE IF EXISTS `labtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `labtype` (
  `id` int NOT NULL AUTO_INCREMENT,
  `labName` varchar(45) DEFAULT NULL,
  `imageName` varchar(100) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `size` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `labtype`
--

LOCK TABLES `labtype` WRITE;
/*!40000 ALTER TABLE `labtype` DISABLE KEYS */;
INSERT INTO `labtype` VALUES (1,'demo lab','helloworld','This is a demo lab.','0.1'),(2,'Micro Controller lab','mico_controller','lab for microcontroller','0.5');
/*!40000 ALTER TABLE `labtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `log` (
  `sno` int NOT NULL AUTO_INCREMENT,
  `container_id` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workbenchallocation`
--

DROP TABLE IF EXISTS `workbenchallocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `workbenchallocation` (
  `sno` int NOT NULL AUTO_INCREMENT,
  `workbenchId` varchar(60) DEFAULT NULL,
  `bookingId` int DEFAULT NULL,
  `participantId` varchar(45) DEFAULT NULL,
  `containerid` varchar(80) DEFAULT NULL,
  `directorypath` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workbenchallocation`
--

LOCK TABLES `workbenchallocation` WRITE;
/*!40000 ALTER TABLE `workbenchallocation` DISABLE KEYS */;
INSERT INTO `workbenchallocation` VALUES (1,'0',5,'none',NULL,NULL),(2,'0',5,'none',NULL,NULL),(3,'0',5,'none',NULL,NULL),(4,'0',5,'none',NULL,NULL),(5,'0',5,'none',NULL,NULL),(6,'0',5,'none',NULL,NULL),(7,'0',5,'none',NULL,NULL),(8,'0',5,'none',NULL,NULL),(9,'0',5,'none',NULL,NULL),(10,'0',5,'none',NULL,NULL),(11,'0',6,'none',NULL,NULL),(12,'0',6,'none',NULL,NULL),(13,'0',6,'none',NULL,NULL),(14,'0',6,'none',NULL,NULL),(15,'0',6,'none',NULL,NULL),(16,'0',6,'none',NULL,NULL),(17,'0',6,'none',NULL,NULL),(18,'0',6,'none',NULL,NULL),(19,'0',6,'none',NULL,NULL),(20,'0',6,'none',NULL,NULL),(21,'12_0',12,'45',NULL,NULL),(22,'12_1',12,'12',NULL,NULL),(23,'12_2',12,'none',NULL,NULL),(24,'12_3',12,'none',NULL,NULL),(25,'12_4',12,'none',NULL,NULL),(26,'13_0',13,'none',NULL,NULL),(27,'13_1',13,'none',NULL,NULL),(28,'13_2',13,'none',NULL,NULL),(29,'13_3',13,'none',NULL,NULL),(30,'13_4',13,'none',NULL,NULL),(31,'13_5',13,'none',NULL,NULL),(32,'13_6',13,'none',NULL,NULL),(33,'13_7',13,'none',NULL,NULL),(34,'13_8',13,'none',NULL,NULL),(35,'13_9',13,'none',NULL,NULL),(36,'13_10',13,'none',NULL,NULL),(37,'13_11',13,'none',NULL,NULL),(38,'13_12',13,'none',NULL,NULL),(39,'13_13',13,'none',NULL,NULL),(40,'13_14',13,'none',NULL,NULL),(41,'14_0',14,'none',NULL,NULL),(42,'14_1',14,'none',NULL,NULL),(43,'14_2',14,'none',NULL,NULL),(44,'14_3',14,'none',NULL,NULL),(45,'14_4',14,'none',NULL,NULL),(46,'14_5',14,'none',NULL,NULL),(47,'14_6',14,'none',NULL,NULL),(48,'14_7',14,'none',NULL,NULL),(49,'14_8',14,'none',NULL,NULL),(50,'14_9',14,'none',NULL,NULL),(51,'14_10',14,'none',NULL,NULL),(52,'14_11',14,'none',NULL,NULL),(53,'14_12',14,'none',NULL,NULL),(54,'14_13',14,'none',NULL,NULL),(55,'14_14',14,'none',NULL,NULL),(56,'15_0',15,'none',NULL,NULL),(57,'15_1',15,'none',NULL,NULL),(58,'15_2',15,'none',NULL,NULL),(59,'15_3',15,'none',NULL,NULL),(60,'15_4',15,'none',NULL,NULL),(61,'15_5',15,'none',NULL,NULL),(62,'15_6',15,'none',NULL,NULL),(63,'15_7',15,'none',NULL,NULL),(64,'15_8',15,'none',NULL,NULL),(65,'15_9',15,'none',NULL,NULL);
/*!40000 ALTER TABLE `workbenchallocation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-10-09  2:54:40
