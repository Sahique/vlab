import datetime
import pymysql
import container

mydb = pymysql.connect(
  host="localhost",
  user="root",
  password="root",
  db="vlab"
)
try:
    mycursor = mydb.cursor()
    print("DB connected")
except:
    print("DB not connected")


def startlab(schedule):
    # Define the provided start time string
    #start_time_str = "2023-10-28T09:00:32.955Z"
    print(">>>>",schedule)
    start_time_str = schedule[4]
    print(start_time_str)

    # Correct the format by adding a colon between hours and minutes
    start_time_str = start_time_str.replace("T", " ").replace("Z", "")

    # Parse the corrected start time string into a datetime object
    start_time = datetime.datetime.strptime(start_time_str, "%Y-%m-%d %H:%M:%S.%f")

    # Get the current time
    time_difference = start_time - datetime.datetime.utcnow()


    # Check if the time difference is 15 minutes or less
    if time_difference <= datetime.timedelta(minutes=15) and time_difference >= datetime.timedelta():
        # If within 15 minutes, trigger the function
        sql_qry= "UPDATE vlab.container_schedular SET status=%s WHERE id=%s " 
        values=("active",schedule[0])
        mycursor.execute(sql_qry,values)
        mydb.commit()
        print(mycursor.rowcount, "was updated.")


        # id, creator_name, course_name, lab_type, start_date, stop_date, no_of_workbench, status, running_count, participants
        #('6', 'Mohammed Sahique', 'AIM', 'cloud computing', '2023-10-04T15:00:00.000Z', '2023-10-04T16:00:00.000Z', '10', 'inactive', NULL, NULL)
        # above is the example for parameter schedule
        container.startdocker(schedule)
    else:
        print("docker not triggered at this time.")

def stoplab(schedule):
    # Define the provided start time string
    #start_time_str = "2023-10-28T09:00:32.955Z"
    print(">>>>",schedule)
    start_time_str = schedule[4]
    print(start_time_str)

    # Correct the format by adding a colon between hours and minutes
    start_time_str = start_time_str.replace("T", " ").replace("Z", "")

    # Parse the corrected start time string into a datetime object
    start_time = datetime.datetime.strptime(start_time_str, "%Y-%m-%d %H:%M:%S.%f")

    # Get the current time
    time_difference = start_time - datetime.datetime.utcnow()


    # Check if the time difference is 15 minutes or less
    if time_difference <= datetime.timedelta(minutes=15) and time_difference >= datetime.timedelta():
        # If within 15 minutes, trigger the function
        sql_qry= "UPDATE vlab.container_schedular SET status=%s WHERE id=%s " 
        values=("inactive",schedule[0])
        mycursor.execute(sql_qry,values)
        mydb.commit()
        print(mycursor.rowcount, "was updated.")

        sql_qry="SELECT * FROM vlab.workbenchallocation WHERE bookingId=%s "
        mycursor.execute(sql_qry,schedule[0])
        list =  mycursor.fetchall()
        print(list); 
        for x in list: 
            res=container.delete_container(x[4]);    print(res)

    else:
        print("docker not triggered at this time.")


sql_qry="SELECT * FROM vlab.container_schedular WHERE status='inactive' " 
mycursor.execute(sql_qry)
list =  mycursor.fetchall()
print(list);
for x in list:
    startlab(x)

sql_qry="SELECT * FROM vlab.container_schedular WHERE status='active' " 
mycursor.execute(sql_qry)
list =  mycursor.fetchall()
print(list);
for x in list:
    stoplab(x)

