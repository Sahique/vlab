from flask import Flask, render_template, request
#import subprocess
import pymysql
import json
import math
from datetime import datetime, timedelta


app = Flask(__name__)

mydb = pymysql.connect(
  #host="db",
  host="localhost",
  user="root",
  password="root"
)
try:
    mycursor = mydb.cursor()
    print("DB connected")
except:
    print("DB not connected")

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/rundocker', methods=['POST'])
def run_docker():
    try:
        #subprocess.run(['docker', 'run', '--rm', 'my-flask-app'])
        return 'Docker container started successfully!'
    except Exception as e:
        return f'Error: {str(e)}'


def checkslots(bookingData):
    sql_qry="SELECT * FROM vlab.container_schedular WHERE lab_type=%s AND status=%s"
    values=(bookingData['Lab Type'],"inactive",)
    mycursor.execute(sql_qry,values)
    list =  mycursor.fetchall()
    print(list);   
    ack=""
    if (len(list)<=0):
        ack="success"
    else:
        for x in list:  
            print(x)          
            # Define your time slots in the given format
            slot1_start = datetime.strptime(x[4], '%Y-%m-%dT%H:%M:%S.%fZ')
            slot1_end = datetime.strptime(x[5], '%Y-%m-%dT%H:%M:%S.%fZ')

            #slot2_start = datetime.strptime('2023-09-28T11:00:00.000Z', '%Y-%m-%dT%H:%M:%S.%fZ')
            #slot2_end = datetime.strptime('2023-09-28T12:00:00.000Z', '%Y-%m-%dT%H:%M:%S.%fZ')

            # Input date and time to check
            check_time = datetime.strptime(bookingData['Start Date-Time'], '%Y-%m-%dT%H:%M:%S.%fZ')

            # Check if the check_time falls within any slot
            found_slot = None

            if (slot1_start <= check_time <= slot1_end and bookingData['Creator Name'] == x[2]):
                ack="failed"
            elif not (slot1_start <= check_time <= slot1_end and bookingData['Creator Name'] == x[2]):
                ack="success"
            elif  (slot1_start <= check_time <= slot1_end and bookingData['Creator Name'] != x[2]):
                ack="success"
            if (ack=="success"): break
    return ack

################## Booking APIS ##########################
@app.route('/createBooking', methods=['POST'])
def createBooking():
    response={"Ack": "failure"}
    #try:
    data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
    print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
    booking_data = json.loads(data)
    #booking_data2={
    #  "Creator Name": "John mathew",   "Course Name": "Embedded Systems",  "Lab Type": "Micro Controller lab",
    #  "Number of workbench": 15,   "Start Date-Time": "2023-09-28T17:49:32.955Z","End Date-Time": "2023-10-28T17:49:32.955Z"
    #}
    storage_check=checkStroageForImage(2,4,booking_data['Number of workbench'])
    checkforslot=checkslots(booking_data)
    print("strorage_check::::",storage_check,checkforslot)
    input_datetime = datetime.strptime(booking_data['Start Date-Time'], '%Y-%m-%dT%H:%M:%S.%fZ')

    if( input_datetime > datetime.now() ):
        # Add 15 minutes
        new_datetime_after_addition = input_datetime + timedelta(minutes=15)

        # Subtract 15 minutes
        input_stop_datetime = datetime.strptime(booking_data['End Date-Time'], '%Y-%m-%dT%H:%M:%S.%fZ')
        new_datetime_after_subtraction = input_stop_datetime - timedelta(minutes=15)


        if(storage_check['Ack']=="success" and checkforslot=="success"):
            sql_qry= "INSERT INTO vlab.container_schedular (creator_name, course_name, lab_type, start_date, stop_date, no_of_workbench, status,buffer_start_datime,buffer_stop_datime) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s) " 
            values=(booking_data['Creator Name'],booking_data['Course Name'],booking_data['Lab Type'],booking_data['Start Date-Time'],booking_data['End Date-Time'],booking_data['Number of workbench'],"inactive", new_datetime_after_addition,new_datetime_after_subtraction)
            mycursor.execute(sql_qry,values)
            #mydb.commit()
            print(mycursor.rowcount, "was inserted.")
            booking_id = mycursor.lastrowid
            if(mycursor.rowcount>0):    
            
                response['Ack']="Slots booked successfully with id : "+ str(booking_id) 
                for x in range(0,booking_data['Number of workbench']):
                    sql_qry= "INSERT INTO vlab.workbenchallocation ( workbenchId,bookingId, participantId) VALUES (%s,%s,%s) " 
                    values=(str(booking_id)+"_"+str(x),booking_id,"none")
                    mycursor.execute(sql_qry,values)
                #
                mydb.commit()
                print(mycursor.rowcount, "was inserted.")
                #if(mycursor.rowcount>0):    response['Ack']="workbench created"
        else:
            if(storage_check['Ack']=="fail" ):   response['Ack']="resources not available"
            if( checkforslot=="fail"):    response['Ack']="slots over lapping"
    else:
        response['Ack']="wrong date and time."
    return response

@app.route('/editBooking', methods=['POST'])
def editBooking():
    response={"Ack": "failure"}
    #try:
    data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
    print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
    edit_data = json.loads(data)
  
    #{
    #  "id":"2", "Creator Name": "Ana mendis",   "Course Name": "Embedded Systems",  "Lab Type": "HDL lab",
    #  "Number of workbench": 20,    "Start Date-Time": "2023-09-28T10:00:32.955Z",  "End Date-Time": "2023-10-28T11:00:32.955Z"
    #}

    sql_qry= "UPDATE vlab.container_schedular SET creator_name=%s, course_name=%s, lab_type=%s, start_date=%s, stop_date=%s, no_of_workbench=%s WHERE id=%s " 
    values=(edit_data['Creator Name'],edit_data['Course Name'],edit_data['Lab Type'],edit_data['Start Date-Time'],edit_data['End Date-Time'],edit_data['Number of workbench'],edit_data['id'],)
    mycursor.execute(sql_qry,values)
    mydb.commit()
    print(mycursor.rowcount, "was updated.")
    if(mycursor.rowcount>0):    response['Ack']="Booking details updated successfully"
    return response

@app.route('/listBooking', methods=['POST'])
def listBooking():
    # eg:   {"Lab Type": "HDL lab","Creator Name": "Ana mendis"}
    response={"Ack": ""}
    booking_data={
      "Creator Name": "John mathew",
      "Course Name": "Embedded Systems",
      "Lab Type": "Micro Controller lab",
      "Number of workbench": 15,
      "Start Date-Time": "2023-09-22T17:49:32.955Z",
      "End Date-Time": "2023-09-22T17:49:32.955Z"
    }
    #try:
    data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
    print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
    list_data = json.loads(data)

    sql_qry=""; base_qry="SELECT * FROM vlab.container_schedular  " 
    filter_conditions = []
    try:
        if list_data['Creator Name']:   filter_conditions.append(f"creator_name = '{list_data['Creator Name']}'")
    except: pass
    try:
        if list_data['Course Name']:   filter_conditions.append(f"course_name = '{list_data['Course Name']}'")
    except: pass
    try:    
        if list_data['Lab Type']:   filter_conditions.append(f"lab_type = '{list_data['Lab Type']}'")
    except: pass
    try:
        if list_data['Number of workbench']:   filter_conditions.append(f"no_of_workbench = '{list_data['Number of workbench']}'")
    except: pass
    try:
        if list_data['Start Date-Time'] and list_data['End Date-Time']:   filter_conditions.append(f"start_date BETWEEN '{list_data['Start Date-Time']}' AND '{list_data['End Date-Time']}'")
        else: 
            if list_data['Start Date-Time'] :   filter_conditions.append(f"start_date = '{list_data['Start Date-Time']}'")
            if list_data['End Date-Time'] :   filter_conditions.append(f"stop_date = '{list_data['End Date-Time']}'")
    except: pass

    if filter_conditions:
        filter_sql = " AND ".join(filter_conditions)
        sql_qry = f"{base_qry} WHERE {filter_sql}"
    else:
        sql_qry = base_qry

    #values=(list_data['Creator Name'],list_data['Course Name'],list_data['Lab Type'],list_data['Number of workbench'],list_data['Start Date-Time'],list_data['End Date-Time'],list_data['id'],)
    mycursor.execute(sql_qry)
    list =  mycursor.fetchall()
    print(list);    response['Ack'] = list
    return response

@app.route('/deleteBooking', methods=['POST'])
def deleteBooking():
    response={"Ack": ""}
    # {"id":5}
    #try:
    data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
    print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
    delete_data = json.loads(data)
    
    sql_qry= " DELETE FROM vlab.container_schedular WHERE id=%s " 
    values=(delete_data['id'],)
    mycursor.execute(sql_qry,values)
    mydb.commit()
    print(mycursor.rowcount, "was deleted.")
    if(mycursor.rowcount>0): response['Ack']="booking was deleted "
    else: response['Ack']="booking Id not found, please check the booking Id. "
    return response

################## Workbench APIS ##########################

# NOTE: creation of workbenches occurs while booking a lab.
  
@app.route('/listWorkbench', methods=['POST'])
def listWorkbench():
    # eg:   {"Lab Type": "HDL lab","Creator Name": "Ana mendis"}
    response={"Ack": ""}
    listAllocation_data={
      "workbenchId": "",
      "bookingId": 5,
      "participantId": ""
    }
    #try:
    data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
    print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
    list_workbench = json.loads(data)

    sql_qry=""; base_qry="SELECT * FROM vlab.workbenchallocation  " 
    filter_conditions = []
    try:
        if list_workbench['workbenchId']:   filter_conditions.append(f"workbenchId = '{list_workbench['workbenchId']}'")
    except: pass
    try:
        if list_workbench['bookingId']:   filter_conditions.append(f"bookingId = '{list_workbench['bookingId']}'")
    except: pass
    try:    
        if list_workbench['participantId']:   filter_conditions.append(f"participantId = '{list_workbench['participantId']}'")
    except: pass

    if filter_conditions:
        filter_sql = " AND ".join(filter_conditions)
        sql_qry = f"{base_qry} WHERE {filter_sql}"
        print(sql_qry)
    else:
        sql_qry = base_qry

    #values=(list_data['Creator Name'],list_data['Course Name'],list_data['Lab Type'],list_data['Number of workbench'],list_data['Start Date-Time'],list_data['End Date-Time'],list_data['id'],)
    mycursor.execute(sql_qry)
    list =  mycursor.fetchall()
    print(list);    response['Ack'] = list
    return response

@app.route('/allocateWorkbench', methods=['POST'])
def allocateWorkbench():
    response={"Ack": "failure"}
    #try:
    data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
    print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
    allocation_data = json.loads(data)
    #{"bookingId":"5", "participants":[45,12]]}
    
    sql_qry= "SELECT * FROM vlab.workbenchallocation WHERE participantId=%s AND bookingId=%s "
    values=("none",allocation_data['bookingId'],)
    mycursor.execute(sql_qry,values)
    booking_data =  mycursor.fetchall(); 
    if(len(booking_data[0])>len(allocation_data['participants'])):
        i=0;
        for x in allocation_data['participants']:
            sql_qry= "UPDATE vlab.workbenchallocation SET participantId=%s AND directorypath=%s WHERE workbenchId=%s AND bookingId=%s "
            #print(str(allocation_data['bookingId']+str(x)),str(x),"none",allocation_data['bookingId'])
            print(str(x),booking_data[i][1],allocation_data['bookingId'])
            values=(str(x),booking_data[i][1],allocation_data['bookingId'],"C:/Users/"+str(x)+"/workspace/",)
            mycursor.execute(sql_qry,values)
            mydb.commit()
            print(mycursor.rowcount, "was inserted.")
            i=i+1;

            if(mycursor.rowcount>0): response['Ack']=str(i)+" workbenches allocated. "
    return response

@app.route('/deallocateWorkbench', methods=['POST'])
def deallocateWorkbench():
    response={"Ack":""}
    #try:
    data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
    print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
    deallocation_data = json.loads(data)
    #{"workbenchIdandparticipantId":[["12_0","45"],["12_1","85"]]}
    
    for x in deallocation_data['workbenchIdandparticipantId']:
        sql_qry= "UPDATE vlab.workbenchallocation SET  participantId=%s WHERE workbenchId=%s AND participantId=%s "
        values=("none",x[0],x[1])
        mycursor.execute(sql_qry,values)
        mydb.commit()
        print(mycursor.rowcount, "was updated.")
        if(mycursor.rowcount>0): 
            response['Ack']=response['Ack']+" workbench deallocated for participant with id "+x[1]+"  ,"
        print(response)   
        
    if(response['Ack']==""):    
        response['Ack']=="failed"
    return response

################## Lab type APIS ##########################
@app.route('/listLabType', methods=['GET'])
def listLabType():
    response={"Ack": "failure"}
    #try:
    #data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
    #print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
    #list_data = json.loads(data)
   
    sql_qry= "SELECT * FROM vlab.labtype  " 
    mycursor.execute(sql_qry)
    labTypes =  mycursor.fetchall()
    print(labTypes)
    response['Ack']= labTypes
    return response

################## Infrastructure APIS #####################
@app.route('/updateInfra', methods=['POST'])
def updatecorestroage():
    #response={"redirect": ""}
    #try:
    sql_qry= "SELECT * FROM vlab.coresandstorage ORDER BY Sno DESC LIMIT 1  " 
    mycursor.execute(sql_qry)
    response =  mycursor.fetchall();    print(response)
    # {   "coreaction":"add", "corecount":5, "storageaction":"remove", "storagecount":1  }
    #   {"cores":"",    "ram":"",   "storage":""}
    data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
    print(">>",data)        #  s = data.decode()   >>> code to decode byte string b'hello'
    update_data = json.loads(data)
    sql_qry= "INSERT INTO vlab.container_schedular (coreallocated, corecredit, coretoal, storageallocated, storagecredit, stroragetotal, cpu,ram, totalcores, totalstroage) VALUES (%d,%d,%d,%d,%d,%d,%d,%d,%d,%d) " 
    if ((response[0][1]+response[0][3])==response[0][9] and (response[0][4]+response[0][6])==response[0][10]):
        if(update_data['coreaction']=="add"):
            response[0][8]=response[0][8]+update_data['corecount']
        else:
            response[0][8]=response[0][8]-update_data['corecount']    
        
        if(update_data['storageaction']=="add"):
            response[0][9]=response[0][9]+ (update_data['storagecount']*1000)
        else:
            response[0][9]=response[0][9]- (update_data['storagecount']*1000)  
    else: 
        print("Number of allocated cores and stroage does not match")    
    values=(response[0][1],response[0][2],response[0][3],response[0][4],response[0][5],response[0][6],response[0][7],update_data['ram'],update_data['cores'],update_data['storage'],)
    mycursor.execute(sql_qry,values)
    mydb.commit()
    print(mycursor.rowcount, "was inserted.")

def checkStroageForImage(imageStroageSize,coreToImageRatio,noOfWorkbench):
    response={"Ack":""}
    sql_qry= "SELECT * FROM vlab.coresandstorage ORDER BY Sno DESC LIMIT 1  " 
    mycursor.execute(sql_qry)
    storage_info =  mycursor.fetchall();    print(storage_info)
    totalCores=storage_info[0][9]; totalStroage=storage_info[0][10]  # getting core balance and strorage balance.
    noOfCore= math.ceil(noOfWorkbench/coreToImageRatio)
    storageRequired=imageStroageSize*noOfWorkbench
    print(">>>>",totalCores,totalStroage)
    print(">>>>",noOfCore,storageRequired)
    if(totalCores>noOfCore and totalStroage>storageRequired):
        print("core and stroage available"); response['Ack']="success";
    else:
        print("Resources not available");   response['Ack']="fail";
    return response
# API to update the infrastructure eg: no of cores, ram, memory etc

if __name__ == '__main__':
    app.run(debug=True)





























































'''
CREATE PROCEDURE `new_procedure` (
	IN creator_name varchar(45) ,
	IN course_name varchar(45) ,
	IN lab_type varchar(45) ,
	IN start_date varchar(45), 
	IN stop_date varchar(45) ,
	IN no_of_workbench varchar(45), 
	IN status varchar(45) ,
	IN running_count varchar(45), 
	IN participants varchar(200)
)
BEGIN
	INSERT INTO vlab.container_schedular (creator_name, course_name, lab_type, start_date, stop_date, no_of_workbench, status)
    VALUES (creator_name, course_name, lab_type, start_date, stop_date, no_of_workbench, status, running_count, participants) ;
    
    DECLARE counter INT DEFAULT 1;
    WHILE counter <= no_of_workbench
		-- Insert data into the table
		INSERT INTO workbenchallocation (column1, column2, column3)
		VALUES (param1, param2, param3);
		
		SET counter = counter + 1;
    END WHILE;
END


'''